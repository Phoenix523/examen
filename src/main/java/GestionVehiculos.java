import java.util.ArrayList;
import java.util.List;

public class GestionVehiculos {

    List<Vehiculo> listaPrincipal;
    List<Vehiculo> listaSecundaria;

    public GestionVehiculos() {
        listaPrincipal = new ArrayList<>();
        listaSecundaria = new ArrayList<>();
    }

    public void anadirVehiculo(Vehiculo vehiculo) {
        listaPrincipal.add(vehiculo);
        listaSecundaria.add(vehiculo);
    }

    public void eliminarVehiculo(String modelo) {
        listaPrincipal.removeIf(vehiculo -> vehiculo.getModelo().equals(modelo));
    }

    public Vehiculo buscarVehiculo(String modelo) {
        for (Vehiculo vehiculo : listaPrincipal) {
            if (vehiculo.getModelo().equals(modelo)) {
                return vehiculo;
            }
        }
        return null;
    }
}
