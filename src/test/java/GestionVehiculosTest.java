import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GestionVehiculosTest {
    private GestionVehiculos gestionVehiculos;

    @BeforeEach
    void setUp() {
        gestionVehiculos = new GestionVehiculos();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void anadirVehiculo() {
        Vehiculo vehiculo1 = new Vehiculo("modelo1", "audi", 2022);
        Vehiculo vehiculo2 = new Vehiculo("modelo2", "fiat", 2023);
        gestionVehiculos.anadirVehiculo(vehiculo1);
        gestionVehiculos.anadirVehiculo(vehiculo2);
        assertEquals(2, gestionVehiculos.listaPrincipal.size());
        assertEquals("modelo1",gestionVehiculos.listaPrincipal.get(0).getModelo());
        assertEquals("modelo2",gestionVehiculos.listaSecundaria.get(1).getModelo());
        assertEquals("audi",gestionVehiculos.listaPrincipal.get(0).getMarca());
        assertEquals("fiat",gestionVehiculos.listaSecundaria.get(1).getMarca());
    }

}

